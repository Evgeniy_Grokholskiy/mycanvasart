//------------------------------------------------------------------------------------------------------
;

let openTab = function (idToClose, idToOpen, nameOfFormToOpen, nameOfFormToClose) {
        let openTab = document.getElementById(idToOpen);
        let closeTab = document.getElementById(idToClose);
        closeTab.classList.remove('selected');
        if (openTab.classList.contains('selected')) {
            openTab.classList.remove('selected');
            openTab.className += ' selected'
        } else {
            openTab.className += ' selected';
        }
        let toOpenForm = document.getElementById(nameOfFormToOpen);
        let toCloseForm = document.getElementById(nameOfFormToClose);
        toCloseForm.classList.remove('enable');
        if (toOpenForm.classList.contains('enable')) {
            toOpenForm.classList.remove('enable');
            toOpenForm.className += ' enable'
        } else {
            toOpenForm.classList.remove('disable');
            toOpenForm.className += ' enable';
        }
    }

;
//--------------------------------------------------------------------------------------------------

;

let selectFigure = function (openId, closeId1, closeId2) {
    let elToOpen = document.getElementById(openId);
    let elToClose1 = document.getElementById(closeId1);
    let elToClose2 = document.getElementById(closeId2);
    elToOpen.classList.remove('tab');
    elToOpen.className += ' tab';
    elToClose1.classList.remove('tab');
    elToClose2.classList.remove('tab');
}


let selectCanvas = function (selectedCanvas, canvasToCloseId1, canvasToCloseId2, canvasToCloseId3, canvasToCloseId4) {
    let selectedType = document.getElementById(selectedCanvas);
    let typeToClose1 = document.getElementById(canvasToCloseId1);
    let typeToClose2 = document.getElementById(canvasToCloseId2);
    let typeToClose3 = document.getElementById(canvasToCloseId3);
    let typeToClose4 = document.getElementById(canvasToCloseId4);
    selectedType.classList.remove('item_selected');
    selectedType.className += ' item_selected';
    typeToClose1.classList.remove('item_selected');
    typeToClose2.classList.remove('item_selected');
    typeToClose3.classList.remove('item_selected');
    typeToClose4.classList.remove('item_selected');
}

let selectSize = function (selectedSize, unselected1, unselected2, unselected3, unselected4) {
    let selectSize = document.getElementById(selectedSize);
    let sizeToUnselect1 = document.getElementById(unselected1);
    let sizeToUnselect2 = document.getElementById(unselected2);
    let sizeToUnselect3 = document.getElementById(unselected3);
    let sizeToUnselect4 = document.getElementById(unselected4);
    selectSize.classList.remove('size_selected');
    selectSize.className += ' size_selected';
    sizeToUnselect1.classList.remove('size_selected');
    sizeToUnselect2.classList.remove('size_selected');
    sizeToUnselect3.classList.remove('size_selected');
    sizeToUnselect4.classList.remove('size_selected');
}


let squareSizeInPx = ['width:56px;height:56px', 'width:94px;height:94px', 'width:141px;height:141px', 'width:225px;height:225px', 'width:282px;height:282px'];
let rectangleSizeInPx = ['width:113px;height:75px', 'width:141px;height:94px', 'width:169px;height:113px', 'width:225px;height:150px', 'width:282px;height:188px'];
let panoramicSizeInPx = ['width:169px;height:56px', 'width:225px;height:75px', 'width:281px;height:94px', 'width:338px;height:113px', 'width:422px;height:141px'];

let sizeChange = function (figureType) {
    let sizeFields = document.getElementsByClassName('size_guide__nav-size_select-ul-li');
    let squareArray = ['12x12', '20x20', '30x30', '48x48', '60x60'];
    let rectangleArray = ['24x16', '30x20', '36x24', '48x32', '60x40'];
    let panoramicArray = ['12x36', '16x48', '20x60', '24x72', '30x90'];


    if (figureType === 'square') {
        for (let i = 0; i < squareArray.length; i++) {
            sizeFields[i].innerHTML = squareArray[i];
            sizeFields[i].dataset.figure = 'square';
        }
    }
    if (figureType === 'rectangle') {
        for (let i = 0; i < rectangleArray.length; i++) {
            sizeFields[i].innerHTML = rectangleArray[i];
            sizeFields[i].dataset.figure = 'rectangle';
        }
    }
    if (figureType === 'panoramic') {
        for (let i = 0; i < panoramicArray.length; i++) {
            sizeFields[i].innerHTML = panoramicArray[i];
            sizeFields[i].dataset.figure = 'panoramic';
        }
    }
}


let resizeCanvas = function (buttonId, number) {
    let resizeCanvasEl = document.getElementById('size_of_canvas');
    let sizeButtonEl = document.getElementById(buttonId);
    if (sizeButtonEl.dataset.figure === 'square') {
        resizeCanvasEl.setAttribute('style', squareSizeInPx[number]);
    }
    if (sizeButtonEl.dataset.figure === 'rectangle') {
        resizeCanvasEl.setAttribute('style', rectangleSizeInPx[number]);
    }
    if (sizeButtonEl.dataset.figure === 'panoramic') {
        resizeCanvasEl.setAttribute('style', panoramicSizeInPx[number]);
    }
}

let automaticResize = function () {
    let resizeCanvasEl = document.getElementById('size_of_canvas');
    let buttonArray = document.getElementsByClassName('size_guide__nav-size_select-ul-li');
    for (let i = 0; i < buttonArray.length; i++) {
        if (buttonArray[i].classList.contains('size_selected')) {
            if (buttonArray[i].dataset.figure === 'square') {
                resizeCanvasEl.setAttribute('style', squareSizeInPx[i]);
            }
            if (buttonArray[i].dataset.figure === 'rectangle') {
                resizeCanvasEl.setAttribute('style', rectangleSizeInPx[i]);
            }
            if (buttonArray[i].dataset.figure === 'panoramic') {
                resizeCanvasEl.setAttribute('style', panoramicSizeInPx[i]);
            }
        }
    }
}

setInterval(automaticResize, 100);

let setTypeOfCanvas = function (canvasNumber) {
    let canvas = document.getElementById('size_of_canvas');
    if (canvasNumber === 0) {
        canvas.className = 'size_guide__main-sofa-type_of_canvas_with_size shadow_for_canvas';
        canvas.innerHTML = '<div class="canvas"></div>';
    }
    if (canvasNumber === 1) {
        canvas.className = 'piece';
        canvas.innerHTML = '<div class="shadow"><div class="piece-item"></div></div><div class="shadow piece-item--margin"><div class="piece-item"></div></div><div class="shadow"><div class="piece-item"></div></div>';
    }
    if (canvasNumber === 2) {
        canvas.className = 'framed';
        canvas.innerHTML = '<div class="framed-canvas"></div>';
    }
    if (canvasNumber === 3) {
        canvas.className = 'framed_print';
        canvas.innerHTML = '';
    }
    if (canvasNumber === 4) {
        canvas.className = 'matting';
        canvas.innerHTML = '<div class="outer_frame"><div class="inner_frame"><div class="inner-canvas"></div></div></div>';
    }
}